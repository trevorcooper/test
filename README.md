# Documentation Samples for Repositories

<dl>
  <dt><a href="https://github.com/tcooper/test/wiki">GitHub Wiki</a></dt>
    <dd>Individual pages in GitHub Wiki's can have editing limited to contributors. However, anybody can add pages to the GitHub Wiki of a public repository. GitHub Wiki content is also a repository however it must be managed as a separate repo on the client side.</dd>
  <dt><a href="http://tcooper.github.io/test/">GitHub Pages</a></dt>
    <dd>GitHub Pages for Projects are associated with the Project repository in a gh-pages branch. GitHub Pages cannot be edited by others but pull-requests can be issued as for other repository content.</dd>
</dl>